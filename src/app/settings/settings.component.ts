import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

    constructor() {
    }

    completedLanguages = [
        {name: 'Javascript'},
        {name: 'HTML'},
        {name: 'CSS'}
    ];

    user = {
        name: '',
        email: '',
        middleName: '',
        lastName: '',
        dob: '',
        age: null,
        professional: '',
        languages: [],
        gender: '',
        completed: [],
        comment: '',
        upload: []
    };
    uploaded = {
        img: []
    };
    getLanguagesKnown(val) {
        this.user.languages = val;
    }

    pushOrPullIntoArray(val, array) {
        if (array.includes(val)) {
            const index = array.indexOf(val);
            array.splice(index, 1);
        } else {
            array.push(val);
        }
    }
    setDOB(val) {
        console.log(val['value']);
    }

    getFormData(form) {
        // console.log(form);
        console.log(this.user);
    }

    getImageUpload(val) {
        console.log(val);
        this.uploaded.img = val;
        console.log(this.uploaded);
    }



    deleteImg() {
        this.uploaded = {
            img: []
        };
    }
    ngOnInit() {

    }

}

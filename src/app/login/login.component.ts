import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user:any={};
  public invalidLogin=""

  constructor(private router: Router) { }

  ngOnInit() {
  }

  redirectToDashboard(){
    if(this.user.username==="admin"&& this.user.password==="Admin@1234"){
      this.router.navigate(['/app/Analytics']);
    }
    else{
      this.invalidLogin="Please enter admin credentials"
    }
    
}

}

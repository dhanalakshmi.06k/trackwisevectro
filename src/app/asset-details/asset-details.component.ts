import { Component, OnInit } from "@angular/core";
import { ConfigService } from "../../service/config.service";
import { AssetService } from "../../service/asset.service";
import { ExcelService } from "../../service/excel.service";
import * as XLSX from "xlsx";

@Component({
    selector: "app-asset-details",
    templateUrl: "./asset-details.component.html",
    styleUrls: ["./asset-details.component.css"]
})
export class AssetDetailsComponent implements OnInit {
    constructor(
        public appDropDownConfig: ConfigService,
        public assetService: AssetService
    ) {}
    public assetDetails: any = {};
    public assetDetailsById: any = {};
    filterText = "";
    public students: any = [];
    public dutyList = this.appDropDownConfig.assetDropDownConfig.Duty;
    saveAssetDetails() {
        console.log(this.assetDetails);
        this.assetService
            .saveAssetDetails(this.assetDetails)
            .subscribe((data: any) => {
                this.getAllAssets();
            });
    }
    updateAssetdetails() {
        let afterDelete = delete this.assetDetails["__v"];
        this.assetService
            .updateAssetsById(this.assetDetails["_id"], this.assetDetails)
            .subscribe((data: any) => {
                this.getAllAssets();
            });
    }

    getAssetDetailsById(assetDetails) {
        this.assetService
            .getAssetDetailsByMongodbId(assetDetails["_id"])
            .subscribe(assetDetails => {
                this.assetDetails = assetDetails;
            });
    }
    getAllAssets() {
        this.assetService.getAssetsBasedOnRange(0, 10).subscribe(assetList => {
            console.log("******************");
            console.log(assetList);
            this.students = assetList;
        });
    }

    listData: any;
    listIntermediateData: any;
    tableDataObj: any = [];
    cc1ExcelDetails: any;
    uploadCc1ExcelDetails(event) {
        let that = this;
        let tableObjs;
        let input = event.target;
        let reader = new FileReader();
        reader.onload = function() {
            let fileData = reader.result;
            let wb = XLSX.read(fileData, { type: "binary" });
            wb.SheetNames.forEach(function(sheetName) {
                let rowObj = XLSX.utils.sheet_to_json(wb.Sheets[sheetName]);
                let tableObjs = JSON.stringify(rowObj);
                if (tableObjs == undefined || tableObjs == null) {
                    return;
                } else {
                    that.listIntermediateData = JSON.stringify(rowObj);
                    that.saveLogsIntermediate(JSON.stringify(rowObj));
                }
            });
        };
        reader.readAsBinaryString(input.files[0]);
    }

    saveLogsIntermediate(data) {
        this.listData = data;
    }

    saveLogsToDb() {
        console.log("saveCC1DetaisToDB-----------listData-------");
        console.log(this.listData);
        // for(let p=0;p<this.listData.length;p++){
        //     console.log(this.listData[p])
        // }
    }

    ngOnInit() {
        this.getAllAssets();
    }
}

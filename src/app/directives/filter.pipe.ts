import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
    // pure: false will watch the value/array for filter.
    // when the value change it filter the value again. and transform the output.
    // its like dynamic filter.
})

export class FilterPipe implements PipeTransform {
    transform(value: any, filterString, filterProperty): any {
        if (value.length === 0 || filterString === '') {
            return value;
        } else {
            const filteredArray = [];
            for (const item of value) {
                const searchString = filterString.toLowerCase();
                const propertyString = item[filterProperty].toLowerCase();
                if (propertyString.includes(searchString)) {
                    filteredArray.push(item);
                }
            }
            return filteredArray;
        }
    }
}

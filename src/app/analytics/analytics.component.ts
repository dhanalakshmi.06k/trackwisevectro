import { Component, OnInit } from "@angular/core";
import { ConfigService } from "../../service/config.service";
import { AnalyticsService } from "../../service/analytics.service";

@Component({
    selector: "app-analytics",
    templateUrl: "./analytics.component.html",
    styleUrls: ["./analytics.component.css"]
})
export class AnalyticsComponent implements OnInit {
    constructor(
        public appDropDownConfig: ConfigService,
        public analyticsService: AnalyticsService
    ) {}
    public wcoResult;
    public computedDetails: any = {};
    public analytics: any = {
        WCO: 0,
        OT: 0,
        volume: 0
    };

    getFormData() {
        let result = this.analyticsService.getWCO(
            this.analytics["WCO"],
            this.analytics["OT"]
        );
        this.wcoResult = result;
        console.log(this.wcoResult);
    }

    getVoulemDetails() {
        let vol = this.analyticsService.getVolume(
            this.wcoResult[0],
            this.analytics["volume"]
        );
        console.log(vol);
        this.computedDetails = vol;
    }
    ngOnInit() {}
}

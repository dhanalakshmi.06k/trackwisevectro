import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})
export class AppURLService {
    public appConfig = {
        appBaseUrl: "http://54.164.135.166:5233/"
    };
    constructor() {}
}

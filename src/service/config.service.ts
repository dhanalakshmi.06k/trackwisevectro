import { Injectable } from "@angular/core";

@Injectable()
export class ConfigService {
    public assetDropDownConfig = {
        Duty: [
            "GSU",
            "Transmission",
            "Distribution",
            "Inductance",
            "Rectifier",
            " Arc Furnace",
            "Industrial",
            "Other",
            "Unknown"
        ],
        typeOfPaper: [
            "IEEE 55 - Kraft",
            "IEEE 65 - T U Kraft",
            "IEC 65 - Kraft",
            "NOMEX",
            "Hybride",
            "Other",
            "Unknown"
        ],
        OilTypeMainTank: [
            "Mineral",
            "Naphthenic",
            "Paraffinic",
            "High Flammability Point",
            "Natural Ester",
            "Synthetic Ester",
            "Silicone",
            "Other",
            "Unknown"
        ],
        Specification: ["IEEE 55C", "IEEE 65C", "IEC", "Unknown"]
    };

    public dict = {
        "5-10": ">5",
        "5-20": ">5",
        "5-30": "2.60",
        "5-40": "1.50",
        "5-50": "1.30",
        "5-60": "0.80",
        "5-70": "0.50",
        "5-80": "0.40",
        "5-90": "0.30",
        "5-100": "0.20"
    };

    constructor() {}
}

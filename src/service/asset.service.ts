/**
 * Created by chandru on 29/6/18.
 */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppURLService} from "./app-url.service";

@Injectable()
export class AssetService {
    constructor(private http: HttpClient, public configService: AppURLService) {
    }
    
    saveAssetDetails(assetData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssetDetails', assetData);
    }
    deleteAssetDetailsByMongodbId(assetMongoDbId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'AssetByMongoId/' + assetMongoDbId);
    }

    getAssetPaginationCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allAsset/count');
    }

    // getAssetsBasedOnRange(skip, limit) {
    //     return this.http.get(this.configService.appConfig.appBaseUrl + 'allAsset?skip=' + skip + '&limit=' + limit);
    // }
    getAssetsBasedOnRange(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allAsset/' + skip + '/' + limit);
    }
    getAssetDetailsByMongodbId(assetId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'assetById/' + assetId);
    }
    updateAssetsById(assetId, assetDetails) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'editAssetDetails/' + assetId, assetDetails);
    }

  

}



/**
 *
 * @name script
 * @author MohammedSaleem
 * @fileOverview generic script that links different css functionality across aplication,
 * viz on-button-click,popup.
 */


$(document).ready(function () {
    let parentEle = $('body');
    let fun = {
        dropDown: function () {
            parentEle.on('click', '.dropDown .head ,.multiSelect .head', function (e) {
                e.stopPropagation();
                if (!$(this).hasClass('disabled')) {
                    let init = $(this).data('init');
                    if (init == '0') {
                        $('.dropDown .list, .multiSelect .list').slideUp(100);
                        $('.dropDown .head, .multiSelect .head').data({
                            'init': '0',
                        });
                        $('.dropDown').removeClass('open');
                        $('.multiSelect').removeClass('open');

                        $(this).parents().eq(0).addClass('open');
                        $(this).parents().eq(0).find('.list').slideDown(200);
                        $(this).data({
                            'init': '1',
                        });
                    } else {
                        $(this).parents().eq(0).find('.list').slideUp(200);
                        $(this).data({
                            'init': '0',
                        });
                        let ele = $(this);
                        setTimeout(function () {
                            ele.parents().eq(0).removeClass('open');
                        }, 150);
                    }
                }
            });

            parentEle.on('click', '.dropDown .list li', function (e) {
                e.stopPropagation();
                let value = $(this).text().trim();
                $(this).parents().eq(1).find('.headTitle').text(value);
                $(this).parents().eq(1).find('.headTitle').val(value);
                $(this).parent().slideUp(200);
                $(this).parents().eq(1).find('.head').data({
                    'init': '0',
                });
                let ele = $(this);
                setTimeout(function () {
                    ele.parents().eq(1).removeClass('open');
                }, 150);
            });
        },
        notificationClick: function () {
            parentEle.on('click', 'header .notificationBtn', function (e) {
                e.stopPropagation();
                let init = $(this).data('init');
                if (!$(this).hasClass('active')) {
                    $('.notifications').fadeIn(0, function () {
                        $('.notifications').removeClass('bounceOutRightCustom').addClass('bounceInRightCustom');
                    });
                    $(this).data({
                        'init': '1',
                    }).addClass('active');
                } else {
                    $('.notifications').removeClass('bounceInRightCustom').addClass('bounceOutRightCustom');
                    $(this).data({
                        'init': '0',
                    }).removeClass('active');

                    setTimeout(function () {
                        $('.notifications').fadeOut(0);
                    }, 200);
                }
            });
            // parentEle.on('click', '.notificationList li', function (e) {
            //     e.stopPropagation();
            //     console.log('test')
            //     let init = $(this).data('init');
            //
            //     if (init == '0') {
            //         $(this).find('.actionBtn').slideDown(200);
            //         $(this).data({
            //             'init': '1',
            //         });
            //     } else {
            //         $(this).find('.actionBtn').slideUp(200);
            //         $(this).data({
            //             'init': '0',
            //         });
            //     }
            // });
            //
            // parentEle.on('click', '.notificationList li .actionBtn .btn', function (e) {
            //     e.stopPropagation();
            //     $('.notificationList li .actionBtn').slideUp(200);
            //     $('.notificationList li').data({
            //         'init': '0',
            //     });
            //     $(this).parents().eq(2).addClass('notified');
            // });
        },
        menuClick: function () {
            parentEle.on('click', 'header .menu', function (e) {
                if (!$('.pageSec').hasClass('collapse')) {
                    $('.pageSec').addClass('collapse');
                } else {
                    $('.pageSec').removeClass('collapse');
                }

            });
        },
        selectTableRow: function () {
            parentEle.on('click', '.selectable .dataRow:not(.header), .selectableList li', function (e) {
                e.stopPropagation();
                // console.log('sal');
                $(this).parent().find('li').removeClass('selected');
                $(this).parent().find('.dataRow').removeClass('selected');
                $(this).addClass('selected');
            });
        },
        toggleButton: function () {
            parentEle.on('click', '.toggleBtn:not(.disabled)', function (e) {
                e.stopPropagation();
                let init = $(this).data('init');
                if (init == '0') {
                    $(this).removeClass('off').addClass('on');
                    $(this).data({
                        'init': '1',
                    });
                } else if (init == '1') {
                    $(this).removeClass('on').addClass('off');
                    $(this).data({
                        'init': '0',
                    });
                }
            });
        },
        popup: function (btn, popup) {
            parentEle.on('click', btn, function (e) {
                e.stopPropagation();
                $('.background').fadeIn(300);
                $('.popup').fadeOut(0);
                $(popup).fadeIn(300);
                $('body,html').scrollTop(0);
            });

            parentEle.on('click', '.popup .close', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $('.popup').fadeOut(300);
                $('.background').fadeOut(300);
            });
        },
        accordion: function () {
            parentEle.on('click', '.accordionSec .accordionHead',
                function (e) {
                    let init = $(this).data('init');

                    if (init === 0) {
                        $('.accordionSec').removeClass('expand')
                            .find('.accordionContent').slideUp(300);
                        $('.accordionHead').data({
                            'init': 0,
                        });

                        $(this).parent()
                            .addClass('expand')
                            .find('> .accordionContent').slideDown(300);
                        $(this).data({
                            'init': 1,
                        });
                    } else {
                        $(this).parent()
                            .removeClass('expand')
                            .find('.accordionContent').slideUp(300);
                        $(this).data({
                            'init': 0,
                        });
                    }
                });
            parentEle.on('click', '.accordionSec .closeAccordion',
                function (e) {
                    $('.accordionSec').removeClass('expand')
                        .find('.accordionContent').slideUp(300);
                    $('.accordionHead').data({
                        'init': 0,
                    });
                });
        },
        expandPlanRow: function () {
            parentEle.on('click', '.weeklyPlannerTable .dataRow:not(.plannerHeader) li.name', function (e) {
                e.stopPropagation();
                if ($(this).parents().eq(1).hasClass('expand')) {
                    $(this).parents().eq(1).removeClass('expand');
                } else {
                    $('.weeklyPlannerTable .dataRow').removeClass('expand');
                    $(this).parents().eq(1).addClass('expand');
                }
            });
        },
        defaultClick: function () {
            $(document).click(function () {
                $('.dropDown .list, .multiSelect .list').slideUp(200);
                $('.dropDown .head, .multiSelect .head').data({
                    'init': '0',
                });
                $('.dropDown').removeClass('open');
                $('.multiSelect').removeClass('open');
                // $('.selectable .dataRow, .selectableList li').removeClass('selected');
            });
        },
        preventDefaultClicks: function () {
            let selectors = '.multiSelect, .checkbox, .uploadBox .editBtn, .accordionHead, .expandBtn';
            parentEle.on('click', selectors, function (e) {
                e.stopPropagation();
            });
        },
    };
    fun.dropDown();
    fun.notificationClick();
    fun.menuClick();
    fun.selectTableRow();
    fun.toggleButton();
    fun.popup('.studentBtn', '.studentPopup');
    fun.popup('.addAssetBtn', '.addAssetPopup');
    fun.popup('.editAssetBtn', '.editAssetPopup');
    fun.popup('.deleteAssetBtn', '.deleteAssetPopup');
    fun.popup('.viewAssetBtn', '.viewAssetPopup');
    fun.popup('.addMoreAssetDetailsBtn', '.addMoreAssetDetailsPopup');
    fun.popup('.addAdditionalAssetDetailsBtn', '.addAdditionalAssetPopup');
    fun.popup('.showAssetDetailsBtn', '.showAssetDetails');
    fun.accordion();
    fun.expandPlanRow();
    fun.preventDefaultClicks();
    fun.defaultClick();
});
